#include "Room.h"



Room::Room()
{
}


Room::~Room()
{
}


bool Room::getVisited()
{
	return visited;
}
void Room::setVisited(bool visitNew)
{
	visited = visitNew;
}

string Room::getInfo()
{
	return info;
}
void Room::setInfo(string infoNew)
{
	info = infoNew;
}

string Room::getInfoShort()
{
	return infoShort;
}
void Room::setInfoShort(string infoShortNew)
{
	infoShort = infoShortNew;
}

void Room::printInfoSmart()//Prints the appropriate description for the room
{
	if (getVisited() == 1) //Player has already been here, so print the short version
	{
		cout << getInfoShort() << endl;
	}
	else //Player hasn't been here, print the full version
	{
		printInfo();
	}
}
void Room::printInfo()//Prints the unabridged description of the room
{
	cout << getInfo();
}


string Room::getID()
{
	return id;
}
void Room::setID(string IDNew)
{
	id = IDNew;
}

Room* Room::getNorthExit()
{
	return north;
}
void Room::setNorthExit(Room* northNew)
{
	north = northNew;
}
Room* Room::getSouthExit()
{
	return south;
}
void Room::setSouthExit(Room* southNew)
{
	south = southNew;
}
Room* Room::getEastExit()
{
	return east;
}
void Room::setEastExit(Room* eastNew)
{
	east = eastNew;
}
Room* Room::getWestExit()
{
	return west;
}
void Room::setWestExit(Room* westNew)
{
	west = westNew;
}

void Room::printExits()
{
	cout << "Exits are: " << endl;
	if (north != nullptr)
	{
		cout << "NORTH";
		if (getLockN() == true)
		{
			cout << " - (Locked)";
		}
		cout << endl;
	}
	if (south != nullptr)
	{
		cout << "SOUTH";
		if (getLockS() == true)
		{
			cout << " - (Locked)";
		}
		cout << endl;
		cout << endl;
	}
	if (east != nullptr)
	{
		cout << "EAST";
		if (getLockE() == true)
		{
			cout << " - (Locked)";
		}
		cout << endl;
		cout << endl;
	}
	if (west != nullptr)
	{
		cout << "WEST";
		if (getLockW() == true)
		{
			cout << " - (Locked)";
		}
		cout << endl;
		cout << endl;
	}
}


bool Room::getLockN()
{
	/*
	Gets the status of the north lock as a bool
	Author: Nicholas Perez
	*/
	return lockN;
}
bool Room::getLockS()
{
	/*
	Gets the status of the south lock as a bool
	Author: Nicholas Perez
	*/
	return lockS;
}
bool Room::getLockE()
{
	/*
	Gets the status of the east lock as a bool
	Author: Nicholas Perez
	*/
	return lockE;
}
bool Room::getLockW()
{
	/*
	Gets the status of the west lock as a bool
	Author: Nicholas Perez
	*/
	return lockW;
}

void Room::lockNorth()
{
	/*
	Sets the north lock to true
	Author: Nicholas Perez
	*/
	lockN = true;
}
void Room::lockSouth()
{
	/*
	Sets the south lock to true
	Author: Nicholas Perez
	*/
	lockS = true;
}
void Room::lockEast()
{
	/*
	Sets the east lock to true
	Author: Nicholas Perez
	*/
	lockE = true;
}
void Room::lockWest()
{
	/*
	Sets the west lock to true
	Author: Nicholas Perez
	*/
	lockW = true;
}

void Room::unlockNorth()
{
	/*
	Sets the north lock to false
	Author: Nicholas Perez
	*/
	lockN = false;
}
void Room::unlockSouth()
{
	/*
	Sets the south lock to false
	Author: Nicholas Perez
	*/
	lockS = false;
}
void Room::unlockEast()
{
	/*
	Sets the east lock to false
	Author: Nicholas Perez
	*/
	lockE = false;
}
void Room::unlockWest()
{
	/*
	Sets the west lock to false
	Author: Nicholas Perez
	*/
	lockW = false;
}


void Room::printPopulation()
{
	if (population.length() != 0)
	{
		cout << "Creatures in this room:" << endl;
		for (int x = 0; x < population.length(); x++) //Operates for each person in the population deck
		{
			Person* temp; //Temporary person pointer
			temp = population.dequeue(); //Dequeues a person-pointer from the population deck
			cout << x+1 << ": " << temp->getName() << endl; //Prints the number of the person in the room population, then prints their name and an endl
			population.enqueue(temp); //Requeues the person-pointer into the population deck
		}
	}
}

void Room::lightsOn()
{
	lights = true;
}
void Room::lightsOff()
{
	lights = false;
}
bool Room::getLights()
{
	return lights;
}