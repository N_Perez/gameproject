#include "Item.h"

Item::Item()
{
}


Item::~Item()
{
}


bool Item::isAccessible()
{
	return accessible;
}
void Item::setAccessible(bool accessNew)
{
	accessible = accessNew;
}

bool Item::isVisible()
{
	return visible;
}
void Item::setVisible(bool visibleNew)
{
	visible = visibleNew;
}

bool Item::isObtainable()
{
	return obtainable;
}
void Item::setObtainable(bool obtainableNew)
{
	obtainable = obtainableNew;
}

int Item::getID()
{
	return id;
}
void Item::setID(int IDNew)
{
	id = IDNew;
}

string Item::getIDName()
{
	return idName;
}
void Item::setIDName(string IDNameNew)
{
	idName = IDNameNew;
}

string Item::getName()
{
	return name;
}
void Item::setName(string nameNew)
{
	name = nameNew;
}