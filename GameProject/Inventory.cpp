#include "Inventory.h"

Inventory::Inventory()
{
}


Inventory::~Inventory()
{
}


Item* Inventory::helpGetItem(int itemNum) //returns a pointer to a specific item, as specified by the item number
{
	itemNum = itemNum - 1; //Adjusts the itemNum from a list starting at 1 to one starting at 0

	if (itemNum < 0 || itemNum > length()) //Checks if the itemNum given is 0, negative, or out of bounds, abort. 
	{
		cout << "ERROR: There is no item #";
		if (itemNum < 10)
		{
			cout << setfill('0') << setw(2) << itemNum;
		}
		else
		{
			cout << itemNum;
		}
		cout << "." << endl;
		abort();
	}

	Item temp;
	Item* outItem;
	Link<Item>* tempLink;
	LDeck<Item> tempdeck; // Temp deck for elements we pull while getting to the item we want

	for (int x = 0; x < itemNum; x++) //Pulls the item in the way into a holding deck
	{
		temp = dequeue(); //Pulls an item out
		tempdeck.enqueue(temp); //Puts it in a holding deck
	}

	tempLink = frontLink(); //Gets a pointer to the front link
	outItem = &(tempLink->element); //Gets a pointer to the item

	while (tempdeck.length() > 0) //stuffs the items from the holding deck back into the inventory
	{
		temp = tempdeck.dedeck();
		endeck(temp);
	}

	return outItem;

	//Could rework to avoid the tempdecks and dequeuing via setting outItem equal to the front pointer and then looping outItem=outItem->next until it hits itemNum but this why fix what ain't broke
}

Item Inventory::removeItem(int itemNum) //pulls and returns a specific item in the inventory, as specified by the item number
{
	itemNum = itemNum - 1; //Adjusts the itemNum from a list starting at 1 to one starting at 0

	if (itemNum < 0 || itemNum > length()) //Checks if the itemNum given is 0, negative, or out of bounds, abort. 
	{
		cout << "ERROR: There is no item #";
		if (itemNum < 10)
		{
			cout << setfill('0') << setw(2) << itemNum;
		}
		else
		{
			cout << itemNum;
		}
		cout << "." << endl;
		abort();
	}

	Item temp;
	Item outItem;
	LDeck<Item> tempdeck; // Temp deck for elements we pull while getting to the item we want

	for (int x = 0; x < itemNum; x++) //Pulls the item in the way into a holding deck
	{
		temp = dequeue(); //Pulls an item out
		tempdeck.enqueue(temp); //Puts it in a holding deck
	}

	outItem = dequeue(); //Pulls the item

	while (tempdeck.length() > 0) //stuffs the items from the holding deck back into the inventory
	{
		temp = tempdeck.dedeck();
		endeck(temp);
	}

	return outItem;

}

void Inventory::addItem(Item itemIn)
{
	enqueue(itemIn);
}
