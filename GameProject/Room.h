#pragma once
#ifndef ROOM_H
#define ROOM_H

#include <string>
#include <iostream>
#include "Inventory.h"
#include "Person.h" 


using namespace std;
class Person; //Forward declare Person

class Room
{

protected:
	Room *north, *south, *east, *west; //Exits
	bool lockN, lockS, lockE, lockW; //Exit locks
	bool visited; //Flag for if the player has visited this room
	string info; //Long description for the room, will print on first visit or if [look]ed at
	string infoShort; //Short description for the room, will print on second+ visit. 
	bool lights; //Flag for if player has hit the light switch

	string id; //Internal room name

public:
	Inventory ground; //Inventory for items in the room

	LDeck<Person*> population; //Stores pointers to NPCs in the room 
	Room(); // Constructor
	~Room(); //Destructor

	bool getVisited();
	void setVisited(bool visitNew);

	string getInfo();
	void setInfo(string infoNew);

	string getInfoShort();
	void setInfoShort(string infoShortNew);

	void printInfoSmart(); //Prints the appropriate description for the room
	void printInfo(); //Prints the unabridged description of the room

	string getID();
	void setID(string IDNew);


	Room* getNorthExit();
	void setNorthExit(Room* northNew);
	Room* getSouthExit();
	void setSouthExit(Room* southNew);
	Room* getEastExit();
	void setEastExit(Room* eastNew);
	Room* getWestExit();
	void setWestExit(Room* westNew);
	
	bool getLockN(); //Gets the status of the north lock as a bool
	bool getLockS(); //Gets the status of the south lock as a bool
	bool getLockE(); //Gets the status of the east lock as a bool
	bool getLockW(); //Gets the status of the west lock as a bool

	void lockNorth(); //Sets the north lock to true
	void lockSouth(); //Sets the south lock to true
	void lockEast(); //Sets the east lock to true
	void lockWest(); //Sets the west lock to true

	void unlockNorth(); //Sets the north lock to false
	void unlockSouth(); //Sets the south lock to false
	void unlockEast(); //Sets the east lock to false
	void unlockWest(); //Sets the west lock to false

	void printExits();
	void printPopulation();

	void lightsOn();
	void lightsOff();
	bool getLights();
};

#endif