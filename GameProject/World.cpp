#include "World.h"

#include <iostream>
#include <string>
#include <algorithm> 
#include <functional>
#include <cctype> 


World::World()
{
}


World::~World()
{
}

void World::runMenu()
{
	location->printInfoSmart();

	int code;
	bool needCode = 1;

	while (needCode == 1) //enter the action loop
	{
		cout << endl;
		cout << "Actions:" << endl;
		cout << "[Look]" << endl;
		cout << "[Inventory]" << endl; //enter submenu asking which book to view
		cout << "[Use]" << endl;
		cout << "[Get]" << endl; //enter submenu asking which book to delete
		cout << "[Give]" << endl; //enter submenu asking which book to modify, then which field to modify
		cout << "[Talk" << endl; //also accept [s], confirm y/n
		cout << "Or [Quit]?" << endl; //also accept [q], confirm y/n

		getline(cin, checkInput); // User enters code
		if (!isvalidCode(checkInput)) // Checks if user input is a valid code
		{
			cout << "Please enter only actions in [Brackets]." << endl; //Add joke for user entering [brackets] if there's time
		}
		else
		{
			code = whichCode(checkInput); //Turns the confirmed valid code into an int for the switch
			needCode = 0; // Exit the action loop
		}
	}
}

void World::load()
{

}



bool isvalidCode(string str) //checks a string to see if it's a valid code
{
	/*
	case insensitive
	Valid codes are
	1 [List]
	2 [View]
	3 [Add]
	4 [Delete]
	5 [Update]
	6 [Save]
	7 [Quit]
	8 [Brackets] //Joke
	*/
	const int NUMBEROFCODES = 8;
	const int NUMBEROFALTCODES = 7;
	string codes[NUMBEROFCODES] = { "LIST", "VIEW", "ADD", "DELETE", "UPDATE", "SAVE", "QUIT", "BRACKETS" };
	string altCodes[NUMBEROFALTCODES] = { "L", "V", "A", "D", "U", "S", "Q" }; //Accept just the first letter for these codes
	bool valid = true; //assume a valid

	if (int(str.length()) == 0) //Check for an empty string
	{
		valid = false;
		return valid;
	}

	//Check if there are more than eight characters in the string, the longest of the acceptable codes
	if (int(str.length()) > 8)
	{
		valid = false;
		return valid;
	}

	//Convert string to uppercase
	std::transform(str.begin(), str.end(), str.begin(), ptr_fun<int, int>(toupper));

	bool checking = 1;

	if (str.length() == 1) //If the string might be a one-character alt-code
	{
		for (int x = 0; checking == 1; x++) //Check for each of the alt-codes
		{
			if (str.compare(altCodes[x]) == 0) //if the string compares equal to an alt-code
			{
				valid = true;
				return valid;
			}

			if (x + 1 == NUMBEROFALTCODES) //Hit the end of the alt code list, didn't find a hit
			{
				checking = 0;
			}
		}

		//didn't match the codes, string too short to bother checking the full codes array
		valid = false;
		return valid;
	}

	for (int x = 0; checking == 1; x++) //Check for each code
	{
		if (str.compare(codes[x]) == 0) //if the string compares equal to a code
		{
			valid = true;
			return valid;
		}

		if (x + 1 == NUMBEROFCODES) //Hit the end of the code list, didn't find a hit
		{
			checking = 0;
		}
	}

	//didn't match the codes
	valid = false;
	return valid;
}

int whichCode(string str) //converts a string containing a valid action code into an int for use in the switch
{
	/*
	Valid codes are
	1 [List]
	2 [View]
	3 [Add]
	4 [Delete]
	5 [Update]
	6 [Save]
	7 [Quit]
	8 [Brackets] //Joke
	*/
	const int NUMBEROFCODES = 8;
	const int NUMBEROFALTCODES = 7;

	int code = 0;
	string codes[NUMBEROFCODES] = { "LIST", "VIEW", "ADD", "DELETE", "UPDATE", "SAVE", "QUIT", "BRACKETS" };
	string altCodes[NUMBEROFALTCODES] = { "L", "V", "A", "D", "U", "S", "Q" }; //Accept just the first letter for codes other than the joke one


																			   //Convert string to uppercase
	std::transform(str.begin(), str.end(), str.begin(), ptr_fun<int, int>(toupper));

	bool checking = 1;
	if (str.length() == 1) //If the string's a one-character alt-code
	{
		for (int x = 0; checking == 1; x++) //Check for each of the alt-codes
		{
			if (str.compare(altCodes[x]) == 0) //if the string compares equal to an alt-code
			{
				code = x + 1; //Found the code, set code to indicate which it is. x+1 because the switch starts from 1, rather than 0
				return code;
			}

			if (x + 1 == NUMBEROFALTCODES) //Hit the end of the alt code list, didn't find a hit
			{
				checking = 0;
				//Shouldn't get here, as this function should only take validated codes
			}
		}
		checking = 1;
	}

	for (int x = 0; checking == 1; x++) //Check for each code
	{
		if (str.compare(codes[x]) == 0) //if the string compares equal to a code
		{
			code = x + 1; //Found the code, set code to indicate which it is. x+1 because the switch starts from 1, rather than 0
			return code;
		}

		if (x + 1 == NUMBEROFCODES) //Hit the end of the code list, didn't find a hit
		{
			checking = 0;
			//Shouldn't get here, as this function should only take validated codes
		}
	}

	//didn't match the codes, which is an error. Return a quit code
	return 7;
}