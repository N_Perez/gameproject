#pragma once
#ifndef ITEM_H
#define ITEM_H

#include <string>
#include <iostream>


using namespace std;

class Item
{
protected:
	bool accessible; //Variable to store if the player can access the item
	bool visible; //Variable to store if the item is visible
	bool obtainable; //Variable to store if the item is obtainable; that is to say, if a person can move it into their inventory.
	int id; //Stores the item's ID number
	string idName; //Stores the item's internal name - ex: "key01", "key02"
	string name; //Stores the item's displayed name - ex: "Small Key", "Large Key"


public:
	Item(); //default constructor
	~Item(); //destructor

	bool isAccessible();
	void setAccessible(bool accessNew);

	bool isVisible();
	void setVisible(bool visibleNew);

	bool isObtainable();
	void setObtainable(bool obtainableNew);

	int getID();
	void setID(int IDNew);

	string getIDName();
	void setIDName(string IDNameNew);

	string getName();
	void setName(string nameNew);
};

#endif