#pragma once
#ifndef INVENTORY_H
#define INVENTORY_H

#include "item.h"
#include "ldeque.h"
#include <iomanip>
#include <stdio.h>

using namespace std;

class Inventory : public LDeck<Item>
{
protected:


public:
	Inventory();
	~Inventory();

	Item* helpGetItem(int itemNum); //returns a pointer to a specific item, as specified by "itemNum" (its position in the inventory)

	Item removeItem(int itemNum); //Pulls and returns a specific item, as specified by "itemNum" (Its position in the inventory)
	void addItem(Item itemIn); //Adds an item to the inventory
};

#endif