#pragma once
#ifndef PERSON_H
#define PERSON_H

#include "ldeque.h"
#include "Inventory.h"
#include "Item.h"
#include "Room.h"

using namespace std;

class Room; //Forward declare Room

class Person
{

protected:
	Inventory bag; //inventory
	Room* location;
	string name; //displayed name
	string id; //Internal Name
	string description; //Descriptor, what you get if you look at it
	bool visible; //Determines if an entity can be seen

public:
	Person(); //Default constructor
	~Person(); //Default destructor

	void printInventory();

	void addItem(Item inItem);
	Item removeItem(int itemNum);

	Room* getLocation();
	void setLocation(Room* locationNew);

	void move(Room* locationNew);

	string getName();
	void setName(string nameNew);

	string getID();
	void setID(string IDNew);

	string getDescription();
	void setDescription(string descriptionNew);

	bool isVisible();
	void setVisible(bool visibleNew);
};

#endif