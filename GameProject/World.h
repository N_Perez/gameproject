#pragma once
#ifndef WORLD_H
#define WORLD_H

#include "ldeque.h"
#include "Person.h"
#include "Room.h"

class World
{
	Room* location;
	

public:

	LDeck<Person> population; //Stores all entities in the world
	LDeck<Room> RoomList; //Stores all rooms in the world


	World();
	~World();

	void runMenu();

	void load();

};

#endif