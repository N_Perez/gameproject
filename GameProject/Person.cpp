#include "Person.h"



Person::Person()
{
}


Person::~Person()
{
}


void Person::printInventory()
{
	for (int x = 1; x <= bag.length(); x++)
	{
		cout << "[" << x << "]: " << (bag.helpGetItem(x))->getName() << endl;
	}
}

void Person::addItem(Item inItem)
{
	bag.addItem(inItem);
}
Item Person::removeItem(int itemNum)
{
	return bag.removeItem(itemNum);
}


Room* Person::getLocation()
{
	return location;
}
void Person::setLocation(Room* locationNew)
{
	location = locationNew;
}
void Person::move(Room* locationNew)
{
	setLocation(locationNew);
}


string Person::getName()
{
	return name;
}
void Person::setName(string nameNew)
{
	name = nameNew;
}

string Person::getID()
{
	return id;
}
void Person::setID(string IDNew)
{
	id = IDNew;
}

string Person::getDescription()
{
	return description;
}
void Person::setDescription(string descriptionNew)
{
	description = descriptionNew;
}


bool Person::isVisible()
{
	return visible;
}
void Person::setVisible(bool visibleNew)
{
	visible = visibleNew;
}